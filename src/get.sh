#!/bin/bash -xe

mkdir -p ./dist

echo "Retrieve Natural Earth bathymetry"
pushd ./dist
wget -N https://naciscdn.org/naturalearth/10m/physical/ne_10m_bathymetry_all.zip

unzip -qq -oj ne_10m_bathymetry_all.zip
popd

echo "Done retrieving Natural Earth bathymetry"
