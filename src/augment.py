import json
import os
import copy
from shapely.geometry import shape, Point,Polygon,MultiPolygon


os.chdir('./dist')
print 'Augmenting data'
lines=open('bathymetry.geojsonseq').read().splitlines()
new_json_lines=[]
for line in lines:
    js=json.loads(line)
    p = shape(js['geometry'])
    js['properties']['area_size'] = p.area
    new_line=json.dumps(js)
    new_json_lines.append(new_line)

open('augmented.json','w+').write('\n'.join(new_json_lines))
