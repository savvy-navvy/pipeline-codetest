#!/bin/bash -xe
mkdir -p ./dist
echo "Convert natural earth bathymetry data"

pushd ./dist

touch bathymetry.geojsonseq
for FILE in *.shp
do
    table="${FILE%.shp}"
    echo "converting file: $FILE..."
    ogr2ogr \
      /vsistdout/ \
      "./$FILE" \
      -f GeoJSONSeq \
      -unsetFid \
      -sql "SELECT * FROM $table" \
      -progress \
      >> bathymetry.geojsonseq
done

popd

echo "Done converting natural earth bathymetry data"
