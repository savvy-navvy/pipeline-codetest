#!/bin/bash -xe

echo "Produce mbtiles"

tippecanoe \
  -o tileset.mbtiles \
  --detect-shared-borders \
  --detect-longitude-wraparound \
  -P -Z9 -z9 -B13 -M300000 \
  -Lbathymetry:<(cat ./dist/augmented.json)

echo "Done"
