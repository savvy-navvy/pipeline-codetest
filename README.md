# Pipeline Codetest #

This repository contains an extremely simplified concept of a data pipeline. It uses GDAL, tippecanoe, Python and shapely.

### What does it supposedly do? ###

1. download natural earth bathemtry (12 shp files)
2. convert each one to GeoJSONseq and concat all to one file (11,558 features - so about 1k each)
3. add area size as label on "Feature"
4. convert to mbtileset

### What is wrong? ###

* What are the biggest problems you can identify with the current approach?
* What would you change if it needed to download 10,000,000 shapefiles?
* What would you change if each shp file contained 10,000,000 features?
* How would you automate this such that it runs daily?

### Other solutions ###

Can you explain which of the following would be suited to automate this "pipeline" and why?

* Docker
* Kubernetes
* AWS Step Functions
* Apache Spark
* AWS Batch
* Apache Airflow
* AWS Managed Workflows

### And finally ###

* What solution/architecture would you recommend?
* How would you scale this?
* What question or concept was not addressed with the above questions?



#### Getting set up ####

Get set up with something that has GDAL, python, tippecanoe etc pre-installed.

```
docker run -i -t \
  -v ${PWD}:/opt/test \
  savvynavvy/buildpack \
  /bin/bash

$> cd /opt/test
$> ./main.sh
```