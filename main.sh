#!/bin/bash -xe

rm -fr .dist/

./src/get.sh
./src/convert.sh
python ./src/augment.py
./src/generate-tileset.sh

